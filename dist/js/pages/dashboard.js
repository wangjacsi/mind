/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function () {

  "use strict";

  // Bar charts using inline values
  $('.sparkbar').sparkline('html', {type: 'bar'});

  //Weather Widget
  $('#weather-widget').weather({
		city: 'Venezia, IT',
		tempUnit: 'C',
		displayDescription: true,
		displayMinMaxTemp: true,
		displayWind: true,
		displayHumidity: true
	});

  //Edit Mode
  $(document).on('click', '#editModeSet', function () {
      if($('#editModeSet span').text() == 'OFF'){
        $('#editModeSet').css('background-color', '#aecbf7');
        $('#editModeSet span').text('ON').removeClass('bg-red').addClass('bg-green');
      } else{
        $('#editModeSet').css('background-color', '#f4f4f4');
        $('#editModeSet span').text('OFF').removeClass('bg-green').addClass('bg-red');
      }
  });

  //Make the dashboard widgets sortable Using jquery UI
  $(".connectedSortable").sortable({
    placeholder: "sort-highlight",
    connectWith: ".connectedSortable",
    handle: ".box-header, .nav-tabs",
    forcePlaceholderSize: true,
    zIndex: 999999
  });
  $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

  //jQuery UI sortable for the todo list
  $(".todo-list").sortable({
    placeholder: "sort-highlight",
    handle: ".handle",
    forcePlaceholderSize: true,
    zIndex: 999999
  });

  //bootstrap WYSIHTML5 - text editor
  $(".textarea").wysihtml5();


  //------------ Visitors Location Data: Get Countries ------------------------//
  //jvectormap data
  var posData = {
    "IT": 1256, //Italy
    "ES": 1017, //Spain
    "CA": 889, //Canada
    "AE": 745, //United Arab Emirates
    "FR": 689, //France
    "BR": 654, // Brazil
    "KR": 437, //Korea Republic of
    "US": 398, //USA
    "SA": 125, //Saudi Arabia
    "DE": 65, //Germany
    "CN": 53, //China
    "RU": 32 //Russia
  };

  var posMapVar ={
      container: $('#world-map'),
      map: 'world_mill_en',
      normalizeFunction: 'polynomial',
      backgroundColor: 'transparent',
      regionStyle: {
        initial: {
          fill: '#e4e4e4',
          "fill-opacity": 1,
          stroke: 'none',
          "stroke-width": 0,
          "stroke-opacity": 1
        }
      },
      series: {
        regions: [{
            values: posData,
            scale: ["#7494B5", "#001F3E"],
            normalizeFunction: 'polynomial'
          }]
      },
      onRegionLabelShow: function(e, el, code) {
          return false;
      },
      onRegionTipShow: function(e, el, code){
        if($.isArray(posData)){
          if (!posData[code])
            el.html(el.html()+' (Visitors: 0)');
          else
            el.html(el.html()+' (Visitors: '+posData[code]+')');
        } else {
            el.html(el.html()+' (Visitors: 0)');
        }
      }
  };
  // initial Drawing the map
  var map = new jvm.Map(posMapVar);

  // This is callback function for Ajax
  var datePickCB = function(start, end, label) {
      // For testing Show
      window.alert("You chose: " + start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
      visitRangeSpan(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
      // This is Ajax function Here
      // This is not real data, it means you can modify this function and data layout
      // Get countries data from json file, but you can get this from server by Ajax
      var totalNumCountries = countries.length; //243 countries
      // For Demo, making random Ajax respose data
      var numCountries = Math.floor((Math.random() * 15) + 5);
      var arr = [];
      while(arr.length < numCountries){
          var randomnumber = Math.floor((Math.random() * totalNumCountries) );
          if(arr.indexOf(randomnumber) > -1) continue;
          arr[arr.length] = randomnumber;
      }

      // make position data for vector Map
      var randomVisitNum =[];
      for(var i in arr){
          randomVisitNum[i] = Math.floor((Math.random() * 2000) + 1);
      }
      // Just sorting random number
      randomVisitNum.sort(function(a, b){return b - a});
      posData = [];
      var visitorsData = [];
      for(var i in arr){
          posData[countries[arr[i]].code] = randomVisitNum[i];
          // make random data for demo
          var unique = Math.floor((Math.random() * randomVisitNum[i]));
          var online = Math.floor((Math.random() * unique / 30));
          var sevenDays = '';
          for(var j=0; j<7; j++){
            if(j == 6){
                sevenDays += unique+':'+randomVisitNum[i];
            } else {
                var randomTotalVisit = Math.floor((Math.random() * 2000));
                var randomUnoqueVisit = Math.floor((Math.random() * randomTotalVisit));
                sevenDays += randomUnoqueVisit+':'+randomTotalVisit+',';
            }

          }
          visitorsData[i] = {
            'code': countries[arr[i]].code,
            'name' : countries[arr[i]].name,
            'totalVisit': randomVisitNum[i],
            'uniqueVisit': unique,
            'online':online,
            'sevenDays': sevenDays
        };
      }

      // Refresh the map inserting new data
      refreshWorldMap();
      refreshVisitorTable(visitorsData);
  };

  function refreshWorldMap(){
      posMapVar.series.regions[0].values = posData;
      $('#world-map').html('');
      map = new jvm.Map(posMapVar);
  }

  function refreshVisitorTable(array){
      //make table
      $('#visitpos').html('');
      var htmlWidget = '<table class="table table-striped"><tr><th><i class="fa fa-flag-checkered"></i> Country</th><th><i class="fa fa-bar-chart"></i> This Week</th><th><i class="fa fa-users"></i> Visits</th><th><i class="fa fa-user"></i>Unique</th><th><i class="fa fa-circle text-success"></i> Online</th></tr>';
      var sideWidget = '';
      if(array.length > 0){
          for(var i in array){
              htmlWidget += '<tr><td><span class="flag-icon flag-icon-'+array[i]['code'].toLowerCase()+'"></span> '+array[i]['name']+'</td>';
              htmlWidget += '<td class="sparkbar">'+array[i]['sevenDays']+'</td>';
              htmlWidget += '<td>'+array[i]['totalVisit']+'</td>';
              htmlWidget += '<td>'+array[i]['uniqueVisit']+'</td>';
              htmlWidget += '<td>'+array[i]['online']+'</td>';
              if(i<3){
                  sideWidget += '<div class="description-block"><div><div class="flag-icon flag-icon-'+array[i]['code'].toLowerCase()+' f32"></div></div>';
                  sideWidget += '<span class="description-text">'+array[i]['name']+'</span>';
                  sideWidget += '<h5 class="description-header">'+array[i]['totalVisit']+'</h5></div>';
              }
          }
      } else {
          htmlWidget += '<tr><td>No Visitors</td><td>-</td><td>0</td><td>0</td><td>0</td>';
      }
      htmlWidget += '</table>';

      // Re-Fresh HTML & seven day bar chart
      $('#visitpos').html(htmlWidget);
      $('.sparkbar').sparkline('html', {type: 'bar'});

      // update Top 3 countires of visits side widget
      $('#sideVisits').html('');
      $('#sideVisits').html(sideWidget);
  }

  function visitRangeSpan(str){
      $('#visit-range').html(str);
  }

  $('.daterange').daterangepicker({
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment().subtract(29, 'days'),
    endDate: moment()
  }, datePickCB);
  //------------ Visitors Location Data: Get Countries ------------------------//


  //-------------
  //- PIE CHART -
  //-------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
  var pieChart = new Chart(pieChartCanvas);
  var PieData = [
    {
      value: parseFloat(storageUsage['Remained']),
      color: "#00c0ef",
      highlight: "#00c0ef",
      label: "Remained"
    },
    {
      value: parseFloat(storageUsage['Media']),
      color: "#d2d6de",
      highlight: "#d2d6de",
      label: "Media"
    },
    {
      value: parseFloat(storageUsage['Images']),
      color: "#f39c12",
      highlight: "#f39c12",
      label: "Images"
    },
    {
      value: parseFloat(storageUsage['Email']),
      color: "#00a65a",
      highlight: "#00a65a",
      label: "Email"
    },
    {
      value: parseFloat(storageUsage['Blog']),
      color: "#348AC7",
      highlight: "#348AC7",
      label: "Blog"
    },
    {
      value: parseFloat(storageUsage['Portfolio']),
      color: "#0073b7",
      highlight: "#0073b7",
      label: "Portfolio"
    },
    {
      value: parseFloat(storageUsage['Video']),
      color: "#3D9970",
      highlight: "#3D9970",
      label: "Video"
    },
    {
      value: parseFloat(storageUsage['Profile']),
      color: "#dd4b39",
      highlight: "#dd4b39",
      label: "Profile"
    },
    {
      value: parseFloat(storageUsage['System']),
      color: "#F012BE",
      highlight: "#F012BE",
      label: "System"
    }
  ];
  var pieOptions = {
    //Boolean - Whether we should show a stroke on each segment
    segmentShowStroke: true,
    //String - The colour of each segment stroke
    segmentStrokeColor: "#fff",
    //Number - The width of each segment stroke
    segmentStrokeWidth: 1,
    //Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    //Number - Amount of animation steps
    animationSteps: 100,
    //String - Animation easing effect
    animationEasing: "easeOutBounce",
    //Boolean - Whether we animate the rotation of the Doughnut
    animateRotate: true,
    //Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale: false,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: false,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
    //String - A tooltip template
    tooltipTemplate: "<%=label%>: <%=value %> MB"
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);
  //-----------------
  //- END PIE CHART -
  //-----------------






  /* jQueryKnob */
  $(".knob").knob();





  //Sparkline charts
  var myvalues = [1000, 1200, 920, 927, 931, 1027, 819, 930, 1021];
  $('#sparkline-1').sparkline(myvalues, {
    type: 'line',
    lineColor: '#92c1dc',
    fillColor: "#ebf4f9",
    height: '50',
    width: '80'
  });
  myvalues = [515, 519, 520, 522, 652, 810, 370, 627, 319, 630, 921];
  $('#sparkline-2').sparkline(myvalues, {
    type: 'line',
    lineColor: '#92c1dc',
    fillColor: "#ebf4f9",
    height: '50',
    width: '80'
  });
  myvalues = [15, 19, 20, 22, 33, 27, 31, 27, 19, 30, 21];
  $('#sparkline-3').sparkline(myvalues, {
    type: 'line',
    lineColor: '#92c1dc',
    fillColor: "#ebf4f9",
    height: '50',
    width: '80'
  });

  //The Calender
  $("#calendar").datepicker();

  //SLIMSCROLL FOR CHAT WIDGET
  $('#chat-box').slimScroll({
    height: '250px'
  });

  /* Morris.js Charts */
  // Sales chart
  var area = new Morris.Area({
    element: 'revenue-chart',
    resize: true,
    data: [
      {y: '2011 Q1', item1: 2666, item2: 2666},
      {y: '2011 Q2', item1: 2778, item2: 2294},
      {y: '2011 Q3', item1: 4912, item2: 1969},
      {y: '2011 Q4', item1: 3767, item2: 3597},
      {y: '2012 Q1', item1: 6810, item2: 1914},
      {y: '2012 Q2', item1: 5670, item2: 4293},
      {y: '2012 Q3', item1: 4820, item2: 3795},
      {y: '2012 Q4', item1: 15073, item2: 5967},
      {y: '2013 Q1', item1: 10687, item2: 4460},
      {y: '2013 Q2', item1: 8432, item2: 5713}
    ],
    xkey: 'y',
    ykeys: ['item1', 'item2'],
    labels: ['Item 1', 'Item 2'],
    lineColors: ['#a0d0e0', '#3c8dbc'],
    hideHover: 'auto'
  });
  var line = new Morris.Line({
    element: 'line-chart',
    resize: true,
    data: [
      {y: '2011 Q1', item1: 2666},
      {y: '2011 Q2', item1: 2778},
      {y: '2011 Q3', item1: 4912},
      {y: '2011 Q4', item1: 3767},
      {y: '2012 Q1', item1: 6810},
      {y: '2012 Q2', item1: 5670},
      {y: '2012 Q3', item1: 4820},
      {y: '2012 Q4', item1: 15073},
      {y: '2013 Q1', item1: 10687},
      {y: '2013 Q2', item1: 8432}
    ],
    xkey: 'y',
    ykeys: ['item1'],
    labels: ['Item 1'],
    lineColors: ['#efefef'],
    lineWidth: 2,
    hideHover: 'auto',
    gridTextColor: "#fff",
    gridStrokeWidth: 0.4,
    pointSize: 4,
    pointStrokeColors: ["#efefef"],
    gridLineColor: "#efefef",
    gridTextFamily: "Open Sans",
    gridTextSize: 10
  });

  //Donut Chart
  var donut = new Morris.Donut({
    element: 'sales-chart',
    resize: true,
    colors: ["#3c8dbc", "#f56954", "#00a65a"],
    data: [
      {label: "Download Sales", value: 12},
      {label: "In-Store Sales", value: 30},
      {label: "Mail-Order Sales", value: 20}
    ],
    hideHover: 'auto'
  });

  //Fix for charts under tabs
  $('.box ul.nav a').on('shown.bs.tab', function () {
    area.redraw();
    donut.redraw();
    line.redraw();
  });

  /* The todo list plugin */
  $(".todo-list").todolist({
    onCheck: function (ele) {
      window.console.log("The element has been checked");
      return ele;
    },
    onUncheck: function (ele) {
      window.console.log("The element has been unchecked");
      return ele;
    }
  });

});
